import React, { useRef, useEffect } from "react";
import * as d3 from "d3";

const BallContainer = (props) => {
  // Set up constants
  const BALL_RADIUS = 10;
  const BALL_COLOR = "#FFFFAA";
  const BALL_SPEED = 0.1;

  // Create a ref for the SVG container
  const containerRef = useRef(null);

  useEffect(() => {
    const container = d3.select(containerRef.current);
    const width = container.node().getBoundingClientRect().width;
    const height = container.node().getBoundingClientRect().height;

    // Create an array of ball objects
    const balls = d3.range(props.ballAmount).map(() => {
      return {
        x: Math.random() * width,
        y: Math.random() * height,
        vx: Math.random() * BALL_SPEED * 2 - BALL_SPEED,
        vy: Math.random() * BALL_SPEED * 2 - BALL_SPEED,
        color: BALL_COLOR,
        radius: BALL_RADIUS,
      };
    });

    // Create the balls
    const ballsGroup = container
      .append("g")
      .attr("class", "balls")
      .selectAll("circle")
      .data(balls)
      .enter()
      .append("circle")
      .attr("r", BALL_RADIUS)
      .attr("cx", (d) => d.x)
      .attr("cy", (d) => d.y)
      .style("fill", (d) => d.color)
      .style("pointer-events", "none")
      .style("filter", "url(#blur)");

    // Define a filter for the Gaussian blur
    const defs = container.append("defs");
    const filter = defs
      .append("filter")
      .attr("id", "blur")
      .attr("x", "-50%")
      .attr("y", "-50%")
      .attr("width", "200%")
      .attr("height", "200%");
    filter
      .append("feGaussianBlur")
      .attr("in", "SourceGraphic")
      .attr("stdDeviation", "2");

    // Move the balls
    let mouseX = width / 2; // set initial mouseX and mouseY to the center of the SVG
    let mouseY = height / 2;
    d3.timer(() => {
      balls.forEach((ball, i) => {
        // Check for collisions with other balls
        for (let j = i + 1; j < balls.length; j++) {
          const otherBall = balls[j];
          const dx = otherBall.x - ball.x;
          const dy = otherBall.y - ball.y;
          const distance = Math.sqrt(dx * dx + dy * dy);
          const minDistance = ball.radius + otherBall.radius;
          if (distance < minDistance) {
            // Collision detected, move balls apart
            const overlap = minDistance - distance;
            const angle = Math.atan2(dy, dx);
            const moveX = overlap * Math.cos(angle) * 0.5;
            const moveY = overlap * Math.sin(angle) * 0.5;
            ball.x -= moveX;
            ball.y -= moveY;
            otherBall.x += moveX;
            otherBall.y += moveY;

            // Update velocities
            const angle2 = Math.atan2(
              otherBall.y - ball.y,
              otherBall.x - ball.x
            );
            const sin = Math.sin(angle2);
            const cos = Math.cos(angle2);
            const vx1 = ball.vx * cos + ball.vy * sin;
            const vy1 = ball.vy * cos - ball.vx * sin;
            const vx2 = otherBall.vx * cos + otherBall.vy * sin;
            const vy2 = otherBall.vy * cos - otherBall.vx * sin;
            const vxTotal = vx1 - vx2;
            ball.vx =
              ((BALL_RADIUS - otherBall.radius) * vx1 +
                2 * otherBall.radius * vx2) /
              (BALL_RADIUS + otherBall.radius);
            otherBall.vx = vxTotal + ball.vx;
            ball.vy = vy1;
            otherBall.vy = vy2;
          }
        }
        const dx = mouseX - ball.x;
        const dy = mouseY - ball.y;
        const distance = Math.sqrt(dx * dx + dy * dy);
        if (distance < props.gravityDistance) {
          ball.vx += dx * props.gravityStrenth;
          ball.vy += dy * props.gravityStrenth;
        }
        // Slow down the ball if it moves in a straight line for too long
        const speed = Math.sqrt(ball.vx * ball.vx + ball.vy * ball.vy);
        if (speed > 0) {
          const slowdownFactor = 0.01;
          const slowdown = Math.max(0, slowdownFactor * (speed - 0.1));
          ball.vx -= ball.vx * slowdown;
          ball.vy -= ball.vy * slowdown;
        }
        ball.x += ball.vx;
        ball.y += ball.vy;
        if (ball.x < BALL_RADIUS || ball.x > width - BALL_RADIUS) {
          ball.vx = -ball.vx;
        }
        if (ball.y < BALL_RADIUS || ball.y > height - BALL_RADIUS) {
          ball.vy = -ball.vy;
        }
      });
      ballsGroup.attr("cx", (d) => d.x).attr("cy", (d) => d.y);
    });

    // Update mouseX and mouseY on mousemove
    container.on("mousemove", function (event) {
      [mouseX, mouseY] = d3.pointer(event);
    });

    // Clean up the D3 elements
    return () => {
      ballsGroup.remove();
    };
  }, [props.ballAmount, props.gravityStrenth, props.gravityDistance]);

  return (
    <svg ref={containerRef} width="1800" height="1600">
      <defs>
        <filter id="glow">
          <feGaussianBlur stdDeviation="2.5" result="coloredBlur" />
          <feMerge>
            <feMergeNode in="coloredBlur" />
            <feMergeNode in="SourceGraphic" />
          </feMerge>
        </filter>
      </defs>
      <style>
        {`
        svg {
          background-color: #333333;
        }
        `}
      </style>
    </svg>
  );
};

export default BallContainer;
