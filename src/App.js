import "./App.css";
import BallContainer from "./Ballcontainer";
import { useState } from "react";
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAcERH4Q0j6xkvfyFUa5iKE4OSkG8sjy18",
  authDomain: "atoms-b281d.firebaseapp.com",
  projectId: "atoms-b281d",
  storageBucket: "atoms-b281d.appspot.com",
  messagingSenderId: "905105972601",
  appId: "1:905105972601:web:d694554affd9b6a6e363a8",
};

// Initialize Firebase
initializeApp(firebaseConfig);

function App() {
  const [ballAmount, setBallAmount] = useState(50);
  const [gravityStrenth, setGravityStrenth] = useState(0.008);
  const [gravityDistance, setGravityDistance] = useState(200);
  const [filledInAmount, setFilledInAmount] = useState(0);

  const regenerate = () => {
    if (filledInAmount > 0) {
      setBallAmount(filledInAmount);
    }
  };

  const increaseGravity = () => {
    setGravityStrenth(gravityStrenth + 0.001);
  };

  const decreaseGravity = () => {
    setGravityStrenth(gravityStrenth - 0.001);
  };

  const increaseGravityDistance = () => {
    setGravityDistance(gravityDistance + 50);
  };

  const decreaseGravityDistance = () => {
    if (gravityDistance > 0) {
      setGravityDistance(gravityDistance - 50);
    }
  };

  const reset = () => {
    setBallAmount(50);
    setGravityStrenth(0.008);
    setGravityDistance(200);
  };

  const handleChange = (event) => {
    setFilledInAmount(event);
  };

  return (
    <div className="App">
      <div className="menu">
        <label>Amount of atoms:</label>
        <input
          type="number"
          onChange={(event) => handleChange(event.target.value)}
        ></input>
        <button className="number-of-balls-button" onClick={regenerate}>
          Regenerate
        </button>
        <button onClick={increaseGravity}>Increase gravity strength</button>
        <button onClick={decreaseGravity}>Decrease gravity strength</button>
        <button onClick={increaseGravityDistance}>
          Increase gravity distance
        </button>
        <button onClick={decreaseGravityDistance}>
          Decrease gravity distance
        </button>
        <button onClick={() => reset()}>reset</button>
      </div>
      <BallContainer
        ballAmount={ballAmount}
        gravityStrenth={gravityStrenth}
        gravityDistance={gravityDistance}
      />
    </div>
  );
}

export default App;
